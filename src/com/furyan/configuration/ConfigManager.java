package com.furyan.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.furyan.configuration.items.ItemConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ConfigManager
{
	private File _configFile;
	private GsonBuilder _builder;

	public HashMap<String, IItemConfig> ItemConfigs;

	public ConfigManager(File configFile)
	{
		_builder = new GsonBuilder();
		_builder.setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation();

		_configFile = configFile;

		ItemConfigs = new HashMap<String, IItemConfig>();
	}

	public void LoadConfig()
	{
		Gson gson = _builder.create();

		if (!_configFile.exists())
		{
			GenerateDefaultConfig();

		} else
		{
			System.out.println("=-=-= * Configuration file found! Loading it now...");
			FileInputStream fis;
			try
			{
				fis = new FileInputStream(_configFile);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null)
				{
					sb.append(line);
				}

				String json = sb.toString();

				ItemConfig config = gson.fromJson(json, ItemConfig.class);
				SetConfig(config);

			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void SaveConfig()
	{
		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try
		{
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Configuration file saved...");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void GenerateDefaultConfig()
	{
		System.out.println("=-=-= * Configuration file not found! Creating default configuration file...");

		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try
		{
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Default configuration file created...");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void SaveJsonToFile(File file, String json) throws IOException
	{
		FileOutputStream os;
		os = new FileOutputStream(file);
		os.write(json.getBytes());
		os.close();
	}

	private void SetConfig(ItemConfig config)
	{
		SaveConfig();
	}
}
