package com.furyan.util;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import FuryansLoreAttributes.MyPlugin;
import PluginReference.MC_ArmorSlotType;
import PluginReference.MC_DamageType;
import PluginReference.MC_Entity;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class LoreAttributeManager
{
	private MyPlugin _plugin;
	private Pattern healthRegex;
	private Pattern regenRegex;
	private Pattern attackSpeedRegex;
	private Pattern damageValueRegex;
	private Pattern damageRangeRegex;
	private Pattern dodgeRegex;
	private Pattern critChanceRegex;
	private Pattern critDamageRegex;
	private Pattern lifestealRegex;
	private Pattern armorRegex;
	private Pattern restrictionRegex;
	private Pattern ExperienceRegex;

	private HashMap<String, Timestamp> attackLog;
	private boolean attackSpeedEnabled;

	private Random generator;

	public LoreAttributeManager(MyPlugin plugin)
	{
		_plugin = plugin;
		healthRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.Health.get("keyword") + ")");
		regenRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.Regen.get("keyword") + ")");
		attackSpeedRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.AttackSpeed.get("keyword") + ")");
		damageValueRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.Damage.get("keyword") + ")");
		damageRangeRegex = Pattern.compile("(\\d+)(-)(\\d+)[ ](" + _plugin.config.Damage.get("keyword") + ")");
		dodgeRegex = Pattern.compile("[+](\\d+)[%][ ](" + _plugin.config.Dodge.get("keyword") + ")");
		critChanceRegex = Pattern.compile("[+](\\d+)[%][ ](" + _plugin.config.CriticalChance.get("keyword") + ")");
		critDamageRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.CriticalDamage.get("keyword") + ")");
		lifestealRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.LifeSteal.get("keyword") + ")");
		armorRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.Armor.get("keyword") + ")");
		restrictionRegex = Pattern.compile("(" + _plugin.config.Restriction.get("keyword") + ": )(\\w*)");
		ExperienceRegex = Pattern.compile("[+](\\d+)[ ](" + _plugin.config.Experience.get("keyword") + ")");

		generator = new Random();
	}

	public void ApplyHpBonus(MC_Entity entity)
	{
		int hpToAdd = GetHpBonus(entity);

		if (entity instanceof MC_Player)
		{
			if (((MC_Player) entity).getHealth() > (getBaseHealth((MC_Player) entity) + hpToAdd))
			{
				((MC_Player) entity).setHealth((getBaseHealth((MC_Player) entity) + hpToAdd));
			}
			((MC_Player) entity).setMaxHealth((getBaseHealth((MC_Player) entity) + hpToAdd));
		} else
		{
			entity.setMaxHealth(entity.getMaxHealth() + hpToAdd);
		}
	}

	public int GetHpBonus(MC_Entity entity)
	{
		int hpBonus = 0;
		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher matcher = healthRegex.matcher(allLore);
					if (matcher.find())
					{
						hpBonus += Integer.valueOf(matcher.group(1));
					}
				}
			}
		}
		return hpBonus;
	}

	public int getBaseHealth(MC_Entity entity)
	{
		int hp = Integer.valueOf((String) _plugin.config.Health.get("base-health"));
		return hp;
	}

	public int getRegenBonus(MC_Entity entity)
	{
		Integer regenBonus = 0;
		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher matcher = regenRegex.matcher(allLore);
					if (matcher.find())
					{
						regenBonus += Integer.valueOf(matcher.group(1));
					}
				}
			}
		}
		return regenBonus;
	}

	public int getDamageBonus(MC_Entity entity)
	{
		Integer damageMin = 0;
		Integer damageMax = 0;
		Integer damageBonus = 0;
		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher rangeMatcher = damageRangeRegex.matcher(allLore);
					Matcher valueMatcher = damageValueRegex.matcher(allLore);
					if (rangeMatcher.find())
					{
						damageMin += Integer.valueOf(rangeMatcher.group(1));
						damageMax += Integer.valueOf(rangeMatcher.group(3));
					}
					if (valueMatcher.find())
					{
						damageBonus += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher rangeMatcher = damageRangeRegex.matcher(allLore);
				Matcher valueMatcher = damageValueRegex.matcher(allLore);
				if (rangeMatcher.find())
				{
					damageMin += Integer.valueOf(rangeMatcher.group(1));
					damageMax += Integer.valueOf(rangeMatcher.group(3));
				}
				if (valueMatcher.find())
				{
					damageBonus += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}
		return (int) Math.round(Math.random() * (damageMax - damageMin) + damageMin + damageBonus + getCritDamage(entity));
	}

	public boolean useRangeOfDamage(MC_Entity entity)
	{
		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher rangeMatcher = damageRangeRegex.matcher(allLore);
					if (rangeMatcher.find())
					{
						return true;
					}
				}
			}
		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher rangeMatcher = damageRangeRegex.matcher(allLore);
				if (rangeMatcher.find())
				{
					return true;
				}
			}
		}
		return false;
	}

	public int getCritDamage(MC_Entity entity)
	{
		if (!critAttack(entity))
		{
			return 0;
		}
		Integer damage = 0;

		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = critDamageRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						damage += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();

		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = critDamageRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					damage += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}

		return damage;
	}

	private boolean critAttack(MC_Entity entity)
	{
		Integer chance = getCritChance(entity);

		Integer roll = generator.nextInt(100) + 1;

		if (chance >= roll)
		{
			return true;
		}
		return false;
	}

	public void handleArmorRestriction(MC_Player player)
	{
		List<MC_ItemStack> armor = (List<MC_ItemStack>) player.getArmor();
		if (!(canUse(player, armor.get(MC_ArmorSlotType.BOOTS))))
		{
			if (player.getInventory().contains(null))
			{
				player.getInventory().add(armor.get(MC_ArmorSlotType.BOOTS));
			} else
			{
				player.getWorld().dropItem(armor.get(MC_ArmorSlotType.BOOTS), player.getLocation(), null);
			}
			armor.set(MC_ArmorSlotType.BOOTS, null);
			player.updateInventory();
		}

		if (!(canUse(player, armor.get(MC_ArmorSlotType.CHEST))))
		{
			if (player.getInventory().contains(null))
			{
				player.getInventory().add(armor.get(MC_ArmorSlotType.CHEST));
			} else
			{
				player.getWorld().dropItem(armor.get(MC_ArmorSlotType.CHEST), player.getLocation(), null);
			}
			armor.set(MC_ArmorSlotType.CHEST, null);
			player.updateInventory();
		}

		if (!(canUse(player, armor.get(MC_ArmorSlotType.HAT))))
		{
			if (player.getInventory().contains(null))
			{
				player.getInventory().add(armor.get(MC_ArmorSlotType.HAT));
			} else
			{
				player.getWorld().dropItem(armor.get(MC_ArmorSlotType.HAT), player.getLocation(), null);
			}
			armor.set(MC_ArmorSlotType.HAT, null);
			player.updateInventory();
		}

		if (!(canUse(player, armor.get(MC_ArmorSlotType.LEGS))))
		{
			if (player.getInventory().contains(null))
			{
				player.getInventory().add(armor.get(MC_ArmorSlotType.LEGS));
			} else
			{
				player.getWorld().dropItem(armor.get(MC_ArmorSlotType.LEGS), player.getLocation(), null);
			}
			armor.set(MC_ArmorSlotType.LEGS, null);
			player.updateInventory();
		}
	}

	public boolean canUse(MC_Player player, MC_ItemStack item)
	{
		if (item != null)
		{
			if (!item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = restrictionRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					if (player.hasPermission("loreattributes." + valueMatcher.group(2)))
					{
						return true;
					} else
					{
						if (Boolean.getBoolean((String) _plugin.config.Restriction.get("display-message")))
						{
							player.sendMessage(_plugin.config.Restriction.get("message").toString().replace("%itemname%", item.getFriendlyName()));
						}
						return false;
					}
				}
			}
		}
		return true;
	}

	public int getDodgeBonus(MC_Entity entity)
	{
		Integer dodgeBonus = 0;
		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = dodgeRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						dodgeBonus += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}
		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = dodgeRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					dodgeBonus += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}
		return dodgeBonus;
	}

	public boolean dodgedAttack(MC_Entity entity)
	{
		Integer chance = getDodgeBonus(entity);

		Integer roll = generator.nextInt(100) + 1;

		if (chance >= roll)
		{
			return true;
		}
		return false;
	}

	private int getCritChance(MC_Entity entity)
	{
		Integer chance = 0;

		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = critChanceRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						chance += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = critChanceRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					chance += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}
		return chance;
	}

	public boolean canAttack(String playerName)
	{
		if (!attackSpeedEnabled)
		{
			return true;
		}
		if (!attackLog.containsKey(playerName))
		{
			return true;
		}
		Date now = new Date();
		if (now.after(attackLog.get(playerName)))
		{
			return true;
		}
		return false;
	}

	public void addAttackCooldown(MC_Entity entity)
	{
		if (!attackSpeedEnabled)
		{
			return;
		}
		Timestamp able = new Timestamp((long) (new Date().getTime() + (getAttackCooldown(entity) * 1000L)));

		attackLog.put(entity.getName(), able);
	}

	private double getAttackCooldown(MC_Entity entity)
	{
		if (!attackSpeedEnabled)
		{
			return 0;
		} else
		{
			return (double) ((Double.parseDouble((String) _plugin.config.AttackSpeed.get("base-delay"))) / getAttackSpeed(entity));
		}
	}

	private double getAttackSpeed(MC_Entity entity)
	{
		if (entity == null)
		{
			return 1;
		}

		double speed = 1;

		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = attackSpeedRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						speed += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}
		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = attackSpeedRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					speed += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}

		return speed;
	}

	public int getArmorBonus(MC_Entity entity)
	{
		Integer armor = 0;

		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = armorRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						armor += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}

		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = armorRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					armor += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}
		return armor;
	}

	public int getLifeSteal(MC_Entity entity)
	{
		Integer lifeSteal = 0;

		for (MC_ItemStack item : entity.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher valueMatcher = lifestealRegex.matcher(allLore);
					if (valueMatcher.find())
					{
						lifeSteal += Integer.valueOf(valueMatcher.group(1));
					}
				}
			}

		}
		MC_ItemStack item = null;

		if (entity instanceof MC_Player)
		{
			item = ((MC_Player) entity).getItemInHand();
		}

		if (item != null)
		{
			if (item.getLore() != null && !item.getLore().isEmpty())
			{
				List<String> lore = item.getLore();
				String allLore = lore.toString().toLowerCase();

				Matcher valueMatcher = lifestealRegex.matcher(allLore);
				if (valueMatcher.find())
				{
					lifeSteal += Integer.valueOf(valueMatcher.group(1));
				}
			}
		}
		return lifeSteal;
	}

	public int getExperienceBonus(MC_Entity victim, MC_Entity killer, MC_DamageType damageType)
	{
		int experience = 0;
		for (MC_ItemStack item : killer.getArmor())
		{
			if (item != null)
			{
				if (item.getLore() != null && !item.getLore().isEmpty())
				{
					List<String> lore = item.getLore();
					String allLore = lore.toString().toLowerCase();

					Matcher matcher = ExperienceRegex.matcher(allLore);
					if (matcher.find())
					{
						experience += Integer.valueOf(matcher.group(1));
					}
				}
			}
		}
		return experience;
	}
}
