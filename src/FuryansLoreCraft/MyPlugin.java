package FuryansLoreCraft;

import java.io.File;
import java.util.List;
import java.util.Random;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

import com.furyan.configuration.ConfigManager;
import com.furyan.util.LoreConfig;
import com.furyan.util.LoreCraftCommand;
import com.furyan.util.StartMetrics;

/*
 * Health - Applied on login*, closing inventory*, respawn*, targeting (for mobs).
 * Damage, life steal, attack speed, critical chance, critical damage - Applied on attack.
 * Regen - Applied when player would normally regenerate health.
 * Item Restriction - Checked on inventory close, shooting bow / attacking, and crafting
 * Dodge, Armor - Applied on taking damage from another player or mob
 * Level Required - Checked on inventory close, shooting bow / attacking, and crafting
 */
public class MyPlugin extends PluginBase
{
	public static int MAJOR_VERSION = 0;
	public static int MINOR_VERSION = 12;
	public static String VERSION_TAG = "a";
	public static String PLUGIN_NAME = "FuryansLoreAttributes";
	public static String PLUGIN_TITLE = "Furyan's Lore Attributes";
	public ConfigManager configManager = null;
	public File configFile = null;
	public MC_Server Server = null;
	public static Random rng = new Random();

	public PluginInfo getPluginInfo()
	{
		PluginInfo info = new PluginInfo();
		info.description = "Lore Craft v" + MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG + "";
		info.name = "Lore Craft";
		info.version = MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG;

		return info;

	}

	public void onTick(int tickNumber)
	{
	}

	public void onStartup(MC_Server server)
	{
		Pl3xLibs.getScheduler().scheduleTask("Lore Crafting", new StartMetrics(this.getPluginInfo()), 100);
		System.out.println("=-=-= Adventure Lands Plugin starting up!");
		Server = server;

		Server.registerCommand(new LoreCraftCommand());

		System.out.println("=-=-= * Loading Configuration...");

		File dir = getPluginConfigDirectory();

		configFile = new File(dir, "config.json");

	}

	public static File getPluginConfigDirectory()
	{
		File jar = new File(MyPlugin.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		File dir = new File(jar.getParent() + File.separator + "AdventureLandsLoreAttributes" + File.separator);
		if (!dir.exists())
		{
			dir.mkdirs();
		}

		return dir;
	}

	public void onShutdown()
	{
		System.out.println("=-=-= Adventure Lands Plugin shutting down!");
		// TODO: Save config on shutdown - config.SaveConfig();

	}

	public void onItemCrafted(MC_Player player, MC_ItemStack item)
	{
		// List<ItemConfig> loreItemList = configManager.ItemConfigs.get(item.getFriendlyName());
		//
		// if (loreItemList == null)
		// {
		// return;
		// }
		//
		// double totalWeight = 0;
		// for (ItemConfig itemInList : loreItemList)
		// {
		// totalWeight += itemInList.weight;
		// }
		//
		// double select = rng.nextDouble() * totalWeight, cumul = 0;
		// for (ItemConfig itemInList : loreItemList)
		// {
		//
		// cumul += itemInList.weight;
		// if (select <= cumul)
		// {
		// item.setCustomName(itemInList.Name);
		// item.setLore(itemInList.Lore);
		//
		// break;
		// }
		// }
	}

	public static void ReloadPlugin()
	{
	}
}
